package com.revolut.interview.env;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EnvParamsTest {

    private static final String PORT_PROPERTY = "MONEY_TASK_PORT";
    private static final int DEFAULT_PORT = 8080;

    @AfterEach
    public void after() {
        System.setProperty(PORT_PROPERTY, "");
    }

    @Test
    public void shouldGetDefaultPort() {
        var port = EnvParams.port();
        assertEquals(DEFAULT_PORT, port);
    }

    @Test
    public void shouldGetPropertyPort() {
        System.setProperty(PORT_PROPERTY, "8084");
        var port = EnvParams.port();
        assertEquals(8084, port);
    }

    @Test
    public void shouldGetDefaultPortIfIncorrectFormat() {
        System.setProperty(PORT_PROPERTY, "dummy");
        var port = EnvParams.port();
        assertEquals(DEFAULT_PORT, port);
    }
}

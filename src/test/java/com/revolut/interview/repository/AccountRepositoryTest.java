package com.revolut.interview.repository;

import com.revolut.interview.domain.Account;
import com.revolut.interview.exception.ZeroBalanceException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public class AccountRepositoryTest {

    private AccountRepository accountRepository;

    @BeforeEach
    public void setup() {
        accountRepository = new AccountRepository();
    }

    @Test
    public void shouldCreateAccountWithInitialData() {
        var account = accountRepository.create();
        assertEquals(account.getBalance(), BigDecimal.ZERO);
    }

    @Test
    public void shouldCorrectFindById() {
        var existedAccount = accountRepository.create();
        var optAccount = accountRepository.findById(existedAccount.getId());
        assertTrue(optAccount.isPresent());

        var foundAccount = optAccount.get();
        assertEquals(existedAccount.getId(), foundAccount.getId());
        assertEquals(existedAccount.getBalance(), foundAccount.getBalance());
    }

    @Test
    public void shouldCorrectDeleteById() {
        var existedAccount = accountRepository.create();
        Optional<Account> optDeletedAccount = accountRepository.deleteById(existedAccount.getId());
        assertTrue(optDeletedAccount.isPresent());

        Optional<Account> tryFindDeletedAccount = accountRepository.findById(existedAccount.getId());
        assertTrue(tryFindDeletedAccount.isEmpty());
    }

    @Test
    public void shouldReturnEmptyForDeleteUnknownId() {
        Optional<Account> optDeletedAccount = accountRepository.deleteById(Long.MIN_VALUE);
        assertTrue(optDeletedAccount.isEmpty());
    }

    @Test
    public void shouldReturnEmptyForUnknownId() {
        var optAccount = accountRepository.findById(Long.MIN_VALUE);
        assertFalse(optAccount.isPresent());
    }

    @Test
    public void shouldCorrectFulfillAccount() throws ZeroBalanceException {
        var existedAccount = accountRepository.create();
        var initAmount = existedAccount.getBalance();
        var fulfillAmount = new BigDecimal(100);

        var optUpdatedAccount = accountRepository.fulfillBalance(existedAccount.getId(), fulfillAmount);
        assertTrue(optUpdatedAccount.isPresent());

        var updatedAccount = optUpdatedAccount.get();

        assertEquals(initAmount.add(fulfillAmount), updatedAccount.getBalance());
    }

    @Test
    public void shouldNotFulfillIfBalanceWillBeNegative() {
        assertThrows(ZeroBalanceException.class, () -> {
            var existedAccount = accountRepository.create();
            var fulfillAmount = new BigDecimal(-100);

            accountRepository.fulfillBalance(existedAccount.getId(), fulfillAmount);
        });
    }

    @Test
    public void shouldReturnEmptyForUnknownAccount() throws ZeroBalanceException {
        assertTrue(accountRepository.fulfillBalance(Long.MIN_VALUE, BigDecimal.ZERO).isEmpty());
    }

    @Test
    public void shouldThrowExceptionForNullAmount() {
        assertThrows(IllegalArgumentException.class, () -> {
            var existedAccount = accountRepository.create();
            accountRepository.fulfillBalance(existedAccount.getId(), null);
        });
    }

    @Test
    public void transferShouldThrowExceptionForNullAmount() {
        assertThrows(IllegalArgumentException.class, () -> {
            var firstAccount = accountRepository.create();
            var secondAccount = accountRepository.create();
            accountRepository.transfer(firstAccount.getId(), secondAccount.getId(), null);
        });
    }

    @Test
    public void transferShouldThrowExceptionForZeroAmount() {
        assertThrows(IllegalArgumentException.class, () -> {
            var firstAccount = accountRepository.create();
            var secondAccount = accountRepository.create();
            accountRepository.transfer(firstAccount.getId(), secondAccount.getId(), BigDecimal.ZERO);
        });
    }

    @Test
    public void transferShouldThrowExceptionForNegativeAmount() {
        assertThrows(IllegalArgumentException.class, () -> {
            var firstAccount = accountRepository.create();
            var secondAccount = accountRepository.create();
            accountRepository.transfer(firstAccount.getId(), secondAccount.getId(), BigDecimal.valueOf(-1));
        });
    }

    @Test()
    public void transferShouldThrowExceptionForTheSameAccount() {
        assertThrows(IllegalArgumentException.class, () -> {
            var firstAccount = accountRepository.create();
            accountRepository.transfer(firstAccount.getId(), firstAccount.getId(), BigDecimal.ONE);
        });
    }

    @Test
    public void transferShouldReturnEmptyForUnknownFirstAccount() throws ZeroBalanceException {
        var account = accountRepository.create();
        var result = accountRepository.transfer(Long.MIN_VALUE, account.getId(), BigDecimal.ONE);

        assertTrue(result.isEmpty());
    }

    @Test
    public void transferShouldReturnEmptyForUnknownSecondAccount() throws ZeroBalanceException {
        var account = accountRepository.create();
        var result = accountRepository.transfer(account.getId(), Long.MIN_VALUE, BigDecimal.ONE);

        assertTrue(result.isEmpty());
    }

    @Test
    public void transferShouldThrowZeroExceptionForNegativeAmount() {
        assertThrows(ZeroBalanceException.class, () -> {
            var firstAccount = accountRepository.create();
            var secondAccount = accountRepository.create();
            var amount = new BigDecimal(100);

            accountRepository.transfer(firstAccount.getId(), secondAccount.getId(), amount);
        });
    }

    @Test
    public void shouldCorrectTransferBalance() throws ZeroBalanceException {
        var firstAccount = accountRepository.create();
        var secondAccount = accountRepository.create();
        var initSecondBalance = secondAccount.getBalance();
        var fulfillAmount = new BigDecimal(150);

        var optFulfilledAccount = accountRepository.fulfillBalance(firstAccount.getId(), fulfillAmount);
        assertTrue(optFulfilledAccount.isPresent());

        var fulfilledAccount = optFulfilledAccount.get();
        var transferAmount = new BigDecimal(50);

        var transferResult = accountRepository.transfer(fulfilledAccount.getId(), secondAccount.getId(), transferAmount);
        assertFalse(transferResult.isEmpty());

        assertEquals(fulfillAmount.subtract(transferAmount), transferResult.get(0).getBalance());
        assertEquals(initSecondBalance.add(transferAmount), transferResult.get(1).getBalance());
    }

    @Test
    public void shouldCorrectTransferEdgeBalanceValue() throws ZeroBalanceException {
        var firstAccount = accountRepository.create();
        var secondAccount = accountRepository.create();
        var initSecondBalance = secondAccount.getBalance();
        var fulfillAmount = new BigDecimal(100);

        var optFulfilledAccount = accountRepository.fulfillBalance(firstAccount.getId(), fulfillAmount);
        assertTrue(optFulfilledAccount.isPresent());

        var fulfilledAccount = optFulfilledAccount.get();
        var transferAmount = new BigDecimal(100);

        var transferResult = accountRepository.transfer(fulfilledAccount.getId(), secondAccount.getId(), transferAmount);
        assertFalse(transferResult.isEmpty());

        assertEquals(fulfillAmount.subtract(transferAmount), transferResult.get(0).getBalance());
        assertEquals(initSecondBalance.add(transferAmount), transferResult.get(1).getBalance());
    }
}

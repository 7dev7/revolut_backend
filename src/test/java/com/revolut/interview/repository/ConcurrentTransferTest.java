package com.revolut.interview.repository;

import com.revolut.interview.domain.Account;
import com.revolut.interview.exception.ZeroBalanceException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.atomic.LongAdder;

import static java.time.Duration.ofMinutes;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTimeout;

public class ConcurrentTransferTest {

    private AccountRepository accountRepository;

    @BeforeEach
    public void setup() {
        accountRepository = new AccountRepository();
    }

    /**
     * try to catch deadlock
     */
    @Test
    public void shouldCorrectTransfer() {
        assertTimeout(ofMinutes(2), () -> {
            final var accounts = Arrays.asList(
                    accountRepository.create(),
                    accountRepository.create(),
                    accountRepository.create()
            );

            final var initAmount = new BigDecimal(100);
            accounts.forEach(acc -> fulfill(acc, initAmount));

            final var transferCount = 18_000_000;
            final var transfers = new ArrayList<Runnable>(transferCount);

            for (int i = 0; i < transferCount / 6; i++) {
                transfers.add(transfer(accounts.get(0), accounts.get(1)));
                transfers.add(transfer(accounts.get(0), accounts.get(2)));
                transfers.add(transfer(accounts.get(1), accounts.get(0)));
                transfers.add(transfer(accounts.get(1), accounts.get(2)));
                transfers.add(transfer(accounts.get(2), accounts.get(0)));
                transfers.add(transfer(accounts.get(2), accounts.get(1)));
            }

            final var adder = new LongAdder();
            transfers.parallelStream()
                    .map(r -> {
                        r.run();
                        return null;
                    }).forEach(n -> adder.increment());

            assertEquals(transferCount, adder.sum());
            accounts.forEach(acc -> assertEquals(initAmount, acc.getBalance()));
        });
    }

    private void fulfill(Account account, BigDecimal balance) {
        try {
            accountRepository.fulfillBalance(account.getId(), balance);
        } catch (ZeroBalanceException e) {
            //ignore this exception in this test
        }
    }

    private Runnable transfer(Account from, Account to) {
        return () -> {
            try {
                accountRepository.transfer(from.getId(), to.getId(), BigDecimal.ONE);
            } catch (ZeroBalanceException e) {
                //ignore this exception in this test
            }
        };
    }
}

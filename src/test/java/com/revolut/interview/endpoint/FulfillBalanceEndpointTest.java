package com.revolut.interview.endpoint;

import com.revolut.interview.domain.Account;
import org.eclipse.jetty.http.HttpStatus;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static io.restassured.RestAssured.patch;
import static io.restassured.RestAssured.post;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class FulfillBalanceEndpointTest extends BaseJavalinTest {

    @Test
    public void shouldCorrectFulfillPositiveBalance() {
        var existedAccount = post("/accounts")
                .then()
                .extract()
                .as(Account.class);

        var fulfillAmount = new BigDecimal(200);
        var fulfilledAccount = patch("/accounts/" + existedAccount.getId() + "/fulfill/" + fulfillAmount)
                .then()
                .statusCode(HttpStatus.OK_200)
                .extract()
                .as(Account.class);

        assertEquals(fulfillAmount, fulfilledAccount.getBalance());
    }

    @Test
    public void shouldCorrectFulfillNegativeBalance() {
        var existedAccount = post("/accounts")
                .then()
                .extract()
                .as(Account.class);

        var plusAmount = new BigDecimal(200);
        patch("/accounts/" + existedAccount.getId() + "/fulfill/" + plusAmount);

        var minusAmount = new BigDecimal(-100);
        var fulfilledAccount = patch("/accounts/" + existedAccount.getId() + "/fulfill/" + minusAmount)
                .then()
                .statusCode(HttpStatus.OK_200)
                .extract()
                .as(Account.class);

        assertEquals(new BigDecimal(100), fulfilledAccount.getBalance());
    }

    @Test
    public void shouldReturn400ForBadAmount() {
        var existedAccount = post("/accounts")
                .then()
                .extract()
                .as(Account.class);

        patch("/accounts/" + existedAccount.getId() + "/fulfill/abc")
                .then()
                .statusCode(HttpStatus.BAD_REQUEST_400);
    }

    @Test
    public void shouldReturn404ForUnknownAccount() {
        var fulfillAmount = new BigDecimal(200);
        patch("/accounts/" + Long.MIN_VALUE + "/fulfill/" + fulfillAmount)
                .then()
                .statusCode(HttpStatus.NOT_FOUND_404);
    }

    @Test
    public void shouldReturn400ForNegativeBalance() {
        var existedAccount = post("/accounts")
                .then()
                .extract()
                .as(Account.class);

        var fulfillAmount = new BigDecimal(-200);
        patch("/accounts/" + existedAccount.getId() + "/fulfill/" + fulfillAmount)
                .then()
                .statusCode(HttpStatus.BAD_REQUEST_400);
    }
}

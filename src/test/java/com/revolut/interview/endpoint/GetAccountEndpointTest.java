package com.revolut.interview.endpoint;

import com.revolut.interview.domain.Account;
import org.eclipse.jetty.http.HttpStatus;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.post;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class GetAccountEndpointTest extends BaseJavalinTest {

    @Test
    public void shouldCorrectReturnAccount() {
        var existedAccount = post("/accounts")
                .then()
                .extract()
                .as(Account.class);

        var receivedAccount = get("/accounts/" + existedAccount.getId())
                .then()
                .statusCode(HttpStatus.OK_200)
                .extract()
                .as(Account.class);

        assertEquals(existedAccount.getId(), receivedAccount.getId());
        assertEquals(existedAccount.getBalance(), receivedAccount.getBalance());
    }

    @Test
    public void shouldReturn404ForUnknownAccountNumber() {
        get("/accounts/" + Long.MIN_VALUE)
                .then()
                .statusCode(HttpStatus.NOT_FOUND_404);
    }
}

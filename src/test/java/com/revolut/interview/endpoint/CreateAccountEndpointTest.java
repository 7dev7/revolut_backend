package com.revolut.interview.endpoint;

import org.eclipse.jetty.http.HttpStatus;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.post;
import static org.hamcrest.Matchers.equalTo;

public class CreateAccountEndpointTest extends BaseJavalinTest {

    @Test
    public void shouldCorrectCreateAccount() {
        post("/accounts")
                .then()
                .statusCode(HttpStatus.CREATED_201)
                .body("balance", equalTo(0));
    }
}

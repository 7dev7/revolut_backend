package com.revolut.interview.endpoint;

import com.revolut.interview.domain.Account;
import org.eclipse.jetty.http.HttpStatus;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static io.restassured.RestAssured.patch;
import static io.restassured.RestAssured.post;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TransferBalanceEndpointTest extends BaseJavalinTest {

    @Test
    public void shouldReturn404ForUnknownFirstAccount() {
        var existedAccount = post("/accounts")
                .then()
                .extract()
                .as(Account.class);

        var amount = new BigDecimal(100);

        patch("/accounts/" + Long.MIN_VALUE + "/transfer/" + existedAccount.getId() + "/" + amount)
                .then()
                .statusCode(HttpStatus.NOT_FOUND_404);
    }

    @Test
    public void shouldReturn404ForUnknownSecondAccount() {
        var existedAccount = post("/accounts")
                .then()
                .extract()
                .as(Account.class);

        var amount = new BigDecimal(100);

        patch("/accounts/" + existedAccount.getId() + "/transfer/" + Long.MIN_VALUE + "/" + amount)
                .then()
                .statusCode(HttpStatus.NOT_FOUND_404);
    }

    @Test
    public void shouldReturn400ForTheSameAccounts() {
        var existedAccount = post("/accounts")
                .then()
                .extract()
                .as(Account.class);

        var amount = new BigDecimal(100);

        patch("/accounts/" + existedAccount.getId() + "/transfer/" + existedAccount.getId() + "/" + amount)
                .then()
                .statusCode(HttpStatus.BAD_REQUEST_400);
    }

    @Test
    public void shouldReturn400ForNegativeAmount() {
        var fromAccount = post("/accounts")
                .then()
                .extract()
                .as(Account.class);

        var toAccount = post("/accounts")
                .then()
                .extract()
                .as(Account.class);

        var amount = new BigDecimal(-100);

        patch("/accounts/" + fromAccount.getId() + "/transfer/" + toAccount.getId() + "/" + amount)
                .then()
                .statusCode(HttpStatus.BAD_REQUEST_400);
    }

    @Test
    public void shouldReturn400ForZeroAmount() {
        var fromAccount = post("/accounts")
                .then()
                .extract()
                .as(Account.class);

        var toAccount = post("/accounts")
                .then()
                .extract()
                .as(Account.class);

        patch("/accounts/" + fromAccount.getId() + "/transfer/" + toAccount.getId() + "/" + BigDecimal.ZERO)
                .then()
                .statusCode(HttpStatus.BAD_REQUEST_400);
    }

    @Test
    public void shouldReturn400ForNegativeBalance() {
        var fromAccount = post("/accounts")
                .then()
                .extract()
                .as(Account.class);

        var toAccount = post("/accounts")
                .then()
                .extract()
                .as(Account.class);

        var amount = new BigDecimal(100);

        patch("/accounts/" + fromAccount.getId() + "/transfer/" + toAccount.getId() + "/" + amount)
                .then()
                .statusCode(HttpStatus.BAD_REQUEST_400);
    }

    @Test
    public void shouldCorrectTransferBalance() {
        var fromAccount = post("/accounts")
                .then()
                .extract()
                .as(Account.class);

        var toAccount = post("/accounts")
                .then()
                .extract()
                .as(Account.class);

        var fulfillAmount = new BigDecimal(150);
        patch("/accounts/" + fromAccount.getId() + "/fulfill/" + fulfillAmount);

        var transferAmount = new BigDecimal(70);
        var accounts = patch("/accounts/" + fromAccount.getId() + "/transfer/" + toAccount.getId() + "/" + transferAmount)
                .then()
                .statusCode(HttpStatus.OK_200)
                .extract()
                .as(Account[].class);

        assertEquals(fulfillAmount.subtract(transferAmount), accounts[0].getBalance());
        assertEquals(transferAmount, accounts[1].getBalance());
    }
}

package com.revolut.interview.endpoint;

import com.revolut.interview.JavalinApp;
import io.javalin.Javalin;
import io.restassured.RestAssured;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;

public class BaseJavalinTest {

    protected static final int PORT = 8081;

    protected static Javalin app;

    @BeforeAll
    public static void before() {
        app = JavalinApp.init(PORT);
        RestAssured.port = PORT;
    }

    @AfterAll
    public static void after() {
        app.stop();
    }
}

package com.revolut.interview.endpoint;

import com.revolut.interview.domain.Account;
import org.eclipse.jetty.http.HttpStatus;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.*;

public class DeleteAccountEndpointTest extends BaseJavalinTest {

    @Test
    public void shouldCorrectRemoveAccount() {
        var existedAccount = post("/accounts")
                .then()
                .extract()
                .as(Account.class);

        delete("/accounts/" + existedAccount.getId())
                .then()
                .statusCode(HttpStatus.OK_200);

        get("/accounts/" + existedAccount.getId())
                .then()
                .statusCode(HttpStatus.NOT_FOUND_404);
    }

    @Test
    public void shouldReturn404ForUnknownAccount() {
        delete("/accounts/" + Long.MIN_VALUE)
                .then()
                .statusCode(HttpStatus.NOT_FOUND_404);
    }
}

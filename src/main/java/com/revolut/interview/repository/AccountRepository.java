package com.revolut.interview.repository;

import com.revolut.interview.domain.Account;
import com.revolut.interview.exception.ZeroBalanceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Provides API to manage accounts
 */
public class AccountRepository {

    private static final Logger LOG = LoggerFactory.getLogger(AccountRepository.class);

    private final AtomicLong accountIdSequence = new AtomicLong(0);
    private final Map<Long, Account> accounts = new ConcurrentHashMap<>();
    private final Map<Long, Lock> locks = new ConcurrentHashMap<>();

    /**
     * Creates account with initial data
     *
     * @return created account
     */
    public Account create() {
        var account = new Account(accountIdSequence.incrementAndGet());
        accounts.put(account.getId(), account);
        return account;
    }

    /**
     * Finds account by account ID
     *
     * @param id account ID
     * @return account
     */
    public Optional<Account> findById(long id) {
        return Optional.ofNullable(accounts.get(id));
    }

    /**
     * Deletes account by account ID. Returns empty value if account with particular ID not found
     *
     * @param id account id
     * @return deleted account data
     */
    public Optional<Account> deleteById(long id) {
        return Optional.ofNullable(accounts.remove(id));
    }

    /**
     * Fulfills account balance. Returns empty value if account with particular id not found
     *
     * @param id     account id
     * @param amount fulfills amount
     * @return updated account data
     * @throws ZeroBalanceException when fulfill will lead to negative balance amount
     */
    public Optional<Account> fulfillBalance(long id, BigDecimal amount) throws ZeroBalanceException {
        if (amount == null) {
            throw new IllegalArgumentException("Fulfill amount cannot be null");
        }

        var optAccount = findById(id);
        if (optAccount.isEmpty()) {
            LOG.error("Account with id = " + id + " not found");
            return Optional.empty();
        }

        var account = optAccount.get();
        var accountBalance = account.getBalance();
        var fulfillResult = accountBalance.add(amount);

        if (fulfillResult.compareTo(BigDecimal.ZERO) < 0) {
            throw new ZeroBalanceException("Cannot fulfill account " + id + " - account balance will be negative");
        }

        account.setBalance(fulfillResult);
        LOG.info("Success fulfill account " + id);
        return Optional.of(account);
    }

    /**
     * Performs transfer balance among 2 accounts. Returns empty list if at least one of accounts not found
     *
     * @param fromAccountId from account ID
     * @param toAccountId   to account ID
     * @param amount        transfer amount
     * @return list of updated accounts
     * @throws IllegalArgumentException when amount is empty or accounts are equal
     * @throws ZeroBalanceException     when fulfill will lead to negative balance amount
     */
    public List<Account> transfer(long fromAccountId, long toAccountId, BigDecimal amount) throws ZeroBalanceException {
        validateAmount(amount);
        validateNegativeAmount(amount);
        validateAccounts(fromAccountId, toAccountId);

        var optFrom = findById(fromAccountId);
        if (optFrom.isEmpty()) {
            LOG.error("Account with id = " + fromAccountId + " not found");
            return Collections.emptyList();
        }
        var fromAccount = optFrom.get();

        var optTo = findById(toAccountId);
        if (optTo.isEmpty()) {
            LOG.error("Account with id = " + toAccountId + " not found");
            return Collections.emptyList();
        }
        var toAccount = optTo.get();

        var firstLock = locks.computeIfAbsent(Math.min(fromAccountId, toAccountId), id -> new ReentrantLock());
        var secondLock = locks.computeIfAbsent(Math.max(fromAccountId, toAccountId), id -> new ReentrantLock());

        firstLock.lock();
        secondLock.lock();
        try {
            if (fromAccount.getBalance().subtract(amount).compareTo(BigDecimal.ZERO) < 0) {
                throw new ZeroBalanceException("Cannot transfer balance from account " + fromAccountId + " - account balance will be negative");
            }

            fromAccount.setBalance(fromAccount.getBalance().subtract(amount));
            toAccount.setBalance(toAccount.getBalance().add(amount));

            return Arrays.asList(fromAccount, toAccount);
        } catch (ZeroBalanceException e) {
            throw e;
        } finally {
            secondLock.unlock();
            firstLock.unlock();
        }
    }

    private void validateAmount(BigDecimal amount) {
        if (amount == null || BigDecimal.ZERO.compareTo(amount) == 0) {
            throw new IllegalArgumentException("Cannot transfer empty balance");
        }
    }

    private void validateNegativeAmount(BigDecimal amount) {
        if (BigDecimal.ZERO.compareTo(amount) > 0) {
            throw new IllegalArgumentException("Cannot transfer negative balance");
        }
    }

    private void validateAccounts(long fromAccount, long toAccount) {
        if (fromAccount == toAccount) {
            throw new IllegalArgumentException("Cannot transfer balance to the same account");
        }
    }
}

package com.revolut.interview;

import com.revolut.interview.endpoint.*;
import com.revolut.interview.exception.ZeroBalanceException;
import com.revolut.interview.repository.AccountRepository;
import io.javalin.Javalin;
import io.javalin.core.JavalinConfig;
import io.javalin.core.validation.JavalinValidation;
import io.javalin.http.ExceptionHandler;
import org.eclipse.jetty.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.function.Consumer;

public class JavalinApp {

    private static final Logger LOG = LoggerFactory.getLogger(JavalinApp.class);

    private static final Consumer<JavalinConfig> CONFIG_CONSUMER = (config) -> {
        config.requestLogger((ctx, ms) -> LOG.info("Got request from " + ctx.ip() + " to " + ctx.fullUrl()));
        config.showJavalinBanner = false;
    };

    private static final ExceptionHandler<Exception> DEFAULT_EXCEPTION_HANDLER = (e, ctx) -> {
        ctx.status(HttpStatus.BAD_REQUEST_400);
        ctx.result(e.getMessage());
    };

    public static Javalin init(int port) {
        var app = Javalin.create(CONFIG_CONSUMER).start(port);
        var accountRepository = new AccountRepository();

        app.post(CreateAccountEndpoint.PATH, new CreateAccountEndpoint(accountRepository));
        app.get(GetAccountEndpoint.PATH, new GetAccountEndpoint(accountRepository));
        app.patch(FulfillBalanceEndpoint.PATH, new FulfillBalanceEndpoint(accountRepository));
        app.delete(DeleteAccountEndpoint.PATH, new DeleteAccountEndpoint(accountRepository));
        app.patch(TransferBalanceEndpoint.PATH, new TransferBalanceEndpoint(accountRepository));

        app.exception(ZeroBalanceException.class, DEFAULT_EXCEPTION_HANDLER);
        app.exception(IllegalArgumentException.class, DEFAULT_EXCEPTION_HANDLER);

        JavalinValidation.register(BigDecimal.class, BigDecimal::new);
        return app;
    }
}

package com.revolut.interview.env;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EnvParams {

    private static final Logger LOG = LoggerFactory.getLogger(EnvParams.class);

    private static final int DEFAULT_PORT = 8080;

    /**
     * Returns specified port. Using default value if no port specified in env parameters
     *
     * @return port
     */
    public static int port() {
        String envPort = System.getProperty("MONEY_TASK_PORT");
        int port = DEFAULT_PORT;
        if (envPort != null && !envPort.isEmpty()) {
            try {
                port = Integer.parseInt(envPort);
            } catch (NumberFormatException e) {
                LOG.error("Specified port int env parameter does not a number: " + envPort + ". Using default port");
            }
        }
        return port;
    }
}

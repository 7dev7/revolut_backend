package com.revolut.interview.exception;

/**
 * Indicates zero account balance condition
 */
public class ZeroBalanceException extends Exception {

    public ZeroBalanceException(String message) {
        super(message);
    }
}

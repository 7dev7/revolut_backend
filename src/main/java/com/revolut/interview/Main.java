package com.revolut.interview;

import com.revolut.interview.env.EnvParams;

public class Main {

    public static void main(String[] args) {
        JavalinApp.init(EnvParams.port());
    }
}

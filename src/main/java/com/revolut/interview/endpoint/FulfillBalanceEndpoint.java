package com.revolut.interview.endpoint;

import com.revolut.interview.domain.Account;
import com.revolut.interview.exception.ZeroBalanceException;
import com.revolut.interview.repository.AccountRepository;
import io.javalin.http.Context;
import io.javalin.http.Handler;
import org.eclipse.jetty.http.HttpStatus;
import org.jetbrains.annotations.NotNull;

import java.math.BigDecimal;
import java.util.Optional;

/**
 * Fulfills particular account
 */
public class FulfillBalanceEndpoint implements Handler {

    public static final String PATH = "/accounts/:accountId/fulfill/:amount";

    private final AccountRepository accountRepository;

    public FulfillBalanceEndpoint(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public void handle(@NotNull Context ctx) throws ZeroBalanceException {
        var accountId = ctx.pathParam("accountId", Long.class).get();
        var amount = ctx.pathParam("amount", BigDecimal.class).get();

        Optional<Account> optAccount = accountRepository.fulfillBalance(accountId, amount);

        optAccount.ifPresentOrElse(ctx::json, () -> {
            ctx.status(HttpStatus.NOT_FOUND_404);
            ctx.result("Account not found");
        });
    }
}

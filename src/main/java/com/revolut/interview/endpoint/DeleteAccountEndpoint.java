package com.revolut.interview.endpoint;

import com.revolut.interview.domain.Account;
import com.revolut.interview.repository.AccountRepository;
import io.javalin.http.Context;
import io.javalin.http.Handler;
import org.eclipse.jetty.http.HttpStatus;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

/**
 * Deletes account by ID
 */
public class DeleteAccountEndpoint implements Handler {

    public static final String PATH = "/accounts/:accountId";

    private final AccountRepository accountRepository;

    public DeleteAccountEndpoint(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public void handle(@NotNull Context ctx) {
        var accountId = ctx.pathParam("accountId", Long.class).get();
        Optional<Account> optDeletedAccount = accountRepository.deleteById(accountId);

        optDeletedAccount.ifPresentOrElse(ctx::json, () -> {
            ctx.status(HttpStatus.NOT_FOUND_404);
            ctx.result("Account not found");
        });
    }
}

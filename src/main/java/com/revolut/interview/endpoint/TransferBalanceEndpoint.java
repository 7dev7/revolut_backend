package com.revolut.interview.endpoint;

import com.revolut.interview.domain.Account;
import com.revolut.interview.repository.AccountRepository;
import io.javalin.http.Context;
import io.javalin.http.Handler;
import org.eclipse.jetty.http.HttpStatus;
import org.jetbrains.annotations.NotNull;

import java.math.BigDecimal;
import java.util.List;

/**
 * Transfers balance among 2 accounts
 */
public class TransferBalanceEndpoint implements Handler {

    public static final String PATH = "/accounts/:fromAccountId/transfer/:toAccountId/:amount";

    private final AccountRepository accountRepository;

    public TransferBalanceEndpoint(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public void handle(@NotNull Context ctx) throws Exception {
        long fromAccountId = ctx.pathParam("fromAccountId", Long.class).get();
        long toAccountId = ctx.pathParam("toAccountId", Long.class).get();
        BigDecimal amount = ctx.pathParam("amount", BigDecimal.class).get();

        List<Account> transferResult = accountRepository.transfer(fromAccountId, toAccountId, amount);

        if (transferResult.isEmpty()) {
            ctx.status(HttpStatus.NOT_FOUND_404);
            ctx.result("Account not found");
            return;
        }
        ctx.json(transferResult);
    }
}

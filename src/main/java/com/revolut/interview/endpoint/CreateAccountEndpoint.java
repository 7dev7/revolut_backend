package com.revolut.interview.endpoint;

import com.revolut.interview.repository.AccountRepository;
import io.javalin.http.Context;
import io.javalin.http.Handler;
import org.eclipse.jetty.http.HttpStatus;
import org.jetbrains.annotations.NotNull;

/**
 * Creates new account
 */
public class CreateAccountEndpoint implements Handler {

    public static final String PATH = "/accounts";

    private final AccountRepository accountRepository;

    public CreateAccountEndpoint(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public void handle(@NotNull Context ctx) {
        var account = accountRepository.create();
        ctx.json(account);
        ctx.status(HttpStatus.CREATED_201);
    }
}

package com.revolut.interview.domain;

import java.math.BigDecimal;

public class Account {

    private long id;
    private BigDecimal balance = BigDecimal.ZERO;

    public Account() {
    }

    public Account(long id) {
        this.id = id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }
}
